function changeButtonStyle() {
  var button = document.getElementById("status");
  button.className = "btn btn-success";
  button.innerHTML = 'Executando...';
}

function stopSimulation() {
  var button = document.getElementById("status");
  button.className = "btn btn-danger";
  button.innerHTML = 'Parado';
}

function resetSimulation() {
  var button = document.getElementById("status");
  button.className = "btn btn-info";
  button.innerHTML = 'Não iniciado';
}

function runSimulation() {
  changeButtonStyle();
  
  // Run A* until reaches the end
  while(currentState != "Finished" && currentState != "NoSolution"){
    graph.aEuclidian();
  }
    
  changePathColor();
  stopSimulation();
}

function randomEnv() {
  createRandomEnvironment();
  
  document.getElementById("random").innerHTML = 'Reiniciar';
  
  if(document.getElementById("status").innerHTML != 'Não iniciado'){
     location.reload();
  }
  
  resetSimulation();
}

