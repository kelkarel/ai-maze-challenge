// ------------------------- Graph.js --------------------------
//  This JavaScript code implements all the Graph functions    |
// -------------------------------------------------------------

class Graph {

    // Defining vertex array and the adjacent list
    constructor(noOfVertices){
        this.noOfVertices = noOfVertices;
        this.AdjList = new Map();
    }

    // Add a vertex to the graph
    addVertex(v) {
    	// Initialize the adjacent list with a null array
    	this.AdjList.set(v, []);
    }

    // Get all the unvisited neighbors
    getNeighbors(v) {
        var unvisitedNeighbors = [];
        for (var value of graph.AdjList.get(v)){
          unvisitedNeighbors.push(value);
        }
        return unvisitedNeighbors;
    }

    // Add edge to the graph
    addEdge(v, w) {
      // Get the list for vertex v and put the vertex w denoting edge between v and w
      this.AdjList.get(v).push(w);
    }

    // Remove edge to the graph
    removeEdge(v, w){
        this.AdjList.get(v).pop(w);
    }

    // Prints the vertex and adjacency list
    printGraph() {
    	// Get all the vertices
    	var keys = this.AdjList.keys();

    	// Iterate over the vertices
    	for (var ind of keys){
            // great the corresponding adjacency lists for the vertex
            var values = this.AdjList.get(ind);
            var conc = "";

            // Iterate over the adjacency list and concatenate the values into a string
            for (var ind1 of values)
                conc += "(" + ind1.i + ", " + ind1.j + ")" + " ";

            // Print the vertex and its adjacency list
            console.log("(" + ind.i + ", " + ind.j + ")" + " -> " + conc);
    	}
    }
  
    // A* algorithm
    aEuclidian(){
        // Check if the open set has at least one non visited cell
        if (openSet.length > 0) {
          
          // Check for the best next option
          var winner = 0;
          for (var i = 0; i < openSet.length; i++) {
            if (openSet[i].f < openSet[winner].f) {
              winner = i;
            }
          }
          
          // Put the current cell as the winner of the open set
          var current = openSet[winner];
          
          // Check if this is the end
          if (current === goal) {
            currentState = "Finished"
          }else{
            // Best option moves from openSet to closedSet
            removeFromArray(openSet, current);
            closedSet.push(current);

            // Check all the neighbors
            var neighbors = this.getNeighbors(current);
            for (var i = 0; i < neighbors.length; i++) {
              var neighbor = neighbors[i];

              // Check if this is a valid next cell
              if (!closedSet.includes(neighbor) && !neighbor.visited || neighbor.getType() === "f") {
                var tempG = current.g + heuristic(neighbor, current);

                // Is this a better path than before?
                var newPath = false;
                if (openSet.includes(neighbor)) {
                  if (tempG < neighbor.g) {
                    neighbor.g = tempG;
                    newPath = true;
                  }
                } else {
                  neighbor.g = tempG;
                  newPath = true;
                  openSet.push(neighbor);
                }

                // Check if there is a better path
                if (newPath) {
                  neighbor.h = heuristic(neighbor, goal);
                  neighbor.f = neighbor.g + neighbor.h;
                  neighbor.previous = current;
                }
              }
            }
        }
        } else {
          currentState = "NoSolution";
        }
        
        // Find the path by working backwards
        path = [];
        if(current != undefined){
          var temp = current;
          path.push(temp);
          while (temp.previous) {
            path.push(temp.previous);
            temp = temp.previous;
          }
        }
    }
}