// Function to change the style of the status button
function changeButtonStyle() {
  var button = document.getElementById("status");
  button.className = "btn btn-success";
  button.innerHTML = 'Executando...';
}

// Function to sop the simulation by changing the style of the status button
function stopSimulation() {
  var button = document.getElementById("status");
  button.className = "btn btn-danger";
  button.innerHTML = 'Parado';
}

// Function to reset the simulation by changing the style of the status button
function resetSimulation() {
  var button = document.getElementById("status");
  button.className = "btn btn-info";
  button.innerHTML = 'Não iniciado';
}

// Function to run the dfs algorithm
function runSimulation() {
  changeButtonStyle();
  graph.dfs(start);
  changePathColor();
  stopSimulation();
}

// Function to create a random environment with the start and end points and the obstacles in the grid
function randomEnv() {
  createRandomEnvironment();
  
  document.getElementById("random").innerHTML = 'Reiniciar';
  
  if(document.getElementById("status").innerHTML != 'Não iniciado'){
     location.reload();
  }
  
  resetSimulation();
}

