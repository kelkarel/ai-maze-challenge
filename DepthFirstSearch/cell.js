// ------------------------- Cell.js --------------------------
//  This JavaScript code implements all the Cell functions.   |
// ------------------------------------------------------------


function Cell(i, j) {

  // -------------------------------------
  // |         Cell's attributes         |
  // -------------------------------------

  // General attributes
  this.i = i;
  this.j = j;
  this.walls = [true, true, true, true];
  this.visited = false;
  this.type = undefined;
  this.color = [undefined, undefined, undefined, undefined];
  this.size = 90;

  // Cell's relative position on the grid
  this.xfPixel = (this.i + 1) * w;
  this.yfPixel = (this.j + 1) * w;
  this.xiPixel = this.i * w;
  this.yiPixel = this.j * w;

  // -------------------------------------
  // |          Cell's functions         |
  // -------------------------------------

  // Highlight the cell according to its type.
  this.highlight = function() {
    let x = this.i * w;
    let y = this.j * w;
    noStroke();
    fill(this.color[0], this.color[1], this.color[2], this.color[3]);
    rect(x, y, w, w);
  };

  // Click on the cell and change the colors
  this.click = function(x, y) {
    if (x >= this.xiPixel && x <= this.xfPixel && y >= this.yiPixel && y <= this.yfPixel) {
      if (clicks == 1) {
        this.type = "i";
        setCellColor(this, initialCellColor());
        start = this;
      }else if (clicks == 2){
        this.type = "f";
        setCellColor(this, finalCellColor());
        goal = this;
      }else {
        this.type = "o";
        setCellColor(this, obstacleCellColor());
      }
      this.setVisited();
    }
  }

  // Set the type and change the color of the cell
  this.clicked = function() {
    if (clicks == 1) {
      this.type = "i";
      setCellColor(this, initialCellColor());
      start = this;
    }else if (clicks == 2){
      this.type = "f";
      setCellColor(this, finalCellColor());
      goal = this;
    }else {
      this.type = "o";
      setCellColor(this, obstacleCellColor());
    }
    this.setVisited();
  }

  // Get the i position
  this.getI = function() {
    return this.i;
  }

  // Get the j position
  this.getJ = function() {
    return this.j;
  }

  // Check if the cell was visited
  this.wasVisited = function() {
    return this.visited;
  }

  // Change the cell to visited
  this.setVisited = function() {
    this.visited = true;
  }

  // Get the type of the cell
  this.getType = function() {
    return this.type;
  }

  // Show the cell
  this.show = function() {
    let x = this.i * w;
    let y = this.j * w;

    stroke(1);

    // Draw all the four walls
    if (this.walls[0]) {
      line(x, y, x + w, y);
    }
    if (this.walls[1]) {
      line(x + w, y, x + w, y + w);
    }
    if (this.walls[2]) {
      line(x + w, y + w, x, y + w);
    }
    if (this.walls[3]) {
      line(x, y + w, x, y);
    }

    // Highlight the cell only if it was visited
    if (this.wasVisited()) {
      this.highlight();
    }
  };
}

// Function to change the cell's color
function setCellColor(cell, color) {
  cell.color = color;
}

// Change the cell's color to green
function initialCellColor() {
  return [39, 174, 96, 255];
}

// Change the cell's color to orange
function finalCellColor() {
  return [211, 84, 0, 255];
}

// Change the cell's color to gray
function obstacleCellColor() {
  return [127, 140, 141, 255];
}

// Change the cell's color to blue
function pathCellColor() {
  return [241, 196, 15, 255];
}

// Change the cell's color to yellow
function currentCellColor() {
  return [41, 128, 185, 255];
}