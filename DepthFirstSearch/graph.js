// ------------------------- Graph.js --------------------------
//  This JavaScript code implements all the Graph functions    |
// -------------------------------------------------------------

class Graph {

    // Defining vertex array and the adjacent list
    constructor(noOfVertices){
        this.noOfVertices = noOfVertices;
        this.AdjList = new Map();
    }

    // Add a vertex to the graph
    addVertex(v) {
    	// Initialize the adjacent list with a null array
    	this.AdjList.set(v, []);
    }

    // Get all the unvisited neighbors
    getUnvisitedNeighbors(v) {
        var unvisitedNeighbors = [];
        for (var value of graph.AdjList.get(v)){            
            if(!value.wasVisited() || value.getType() == "f") {
                unvisitedNeighbors.push(value);
            }
        }
        return unvisitedNeighbors;
    }

    // Add edge to the graph
    addEdge(v, w) {
      // Get the list for vertex v and put the vertex w denoting edge between v and w
      this.AdjList.get(v).push(w);
    }

    // Remove edge to the graph
    removeEdge(v, w){
        this.AdjList.get(v).pop(w);
    }

    // Prints the vertex and adjacency list
    printGraph() {
    	// Get all the vertices
    	var keys = this.AdjList.keys();

    	// Iterate over the vertices
    	for (var ind of keys){
            // Great the corresponding adjacency lists for the vertex
            var values = this.AdjList.get(ind);
            var conc = "";

            // Iterate over the adjacency list and concatenate the values into a string
            for (var ind1 of values)
                conc += "(" + ind1.i + ", " + ind1.j + ")" + " ";

            // Print the vertex and its adjacency list
            console.log("(" + ind.i + ", " + ind.j + ")" + " -> " + conc);
    	}
    }

    // Recursive function which process and explore all the adjacent vertex of the vertex with which it is called
    dfs(startVertex) {
        if(startVertex){
            // Change color of the visited cells
            if(startVertex != start && startVertex != goal) {
                startVertex.setVisited();
                setCellColor(startVertex, pathCellColor());
            }
            
            // Check if the stack contains the vertex, if not include the vertex in the stack
            if(!stack.includes(startVertex)) {
                stack.push(startVertex);
            }

            // Get the neighbors
            var neighbors = this.getUnvisitedNeighbors(startVertex);
          
            // Choose a random neighbor to go
            var next = random(neighbors);

            // Remove the walls
            if(next){
                removeWalls(startVertex, next);
            }

            // If does not exist any available neighbor, remove the last element of the stack and change the next cell to the last element of the stack
            if(neighbors.length == 0) {
                stack.pop();
                next = stack[stack.length-1];
            }
          
            // Only run the dfs if the next cell is not the goal
            if(next != goal) {
                this.dfs(next);
            }else{
                stack.push(goal);
            }
        }
    }
}