// ---------- Maze generator ----------
// Author: Karel Miranda
// Project: Challenge 2: Artificial Intelligence
// ------------------------------------

// ----------------- Maze.js -----------------
//  This JavaScript code creates the maze    |
// -------------------------------------------

// Variables
let cols, rows;
let w = 90;
let grid = [];
let size = w*10;
let clicks = 0;
let scale = (size/w);
let start, goal, current;
var graph = new Graph(scale*scale);
var stack = [];
var count = 0;

// Put cells on the grid
function setup() {

  createCanvas(size, size);
  cols = floor(width / w);
  rows = floor(height / w);
  
  for (let j = 0; j < rows; j++) {
    for (let i = 0; i < cols; i++) {
      var cell = new Cell(i, j);
      grid.push(cell);
    }
  }
  createGraph();
}

// Show each cell on the grid
function draw() {
  background(51);
  for (let i = 0; i < grid.length; i++) {
    grid[i].show();
  }
}

// Create a undirected graph
function createGraph() {
  var vertices = [];

  // Adding grid cells to the graph vertices
  for (var i = 0; i < grid.length; i++) {
    vertices[i] = grid[i];
  	graph.addVertex(vertices[i]);
  }

  // Adding edges to each cell
  for (var idx = 0; idx < vertices.length; idx++) {    
    // Add the up vertice
    if (vertices[idx].j - 1 >= 0) {
      graph.addEdge(vertices[idx], vertices[idx-scale]);
    }
    // Add the right vertice
    if (vertices[idx].i + 1 < (size/w)) {
      graph.addEdge(vertices[idx], vertices[idx+1]);
    }
    // Add the down vertice
    if (vertices[idx].j + 1 < (size/w)) {
      graph.addEdge(vertices[idx], vertices[idx+scale]);
    }
    // Add the left vertice
    if (vertices[idx].i - 1 >= 0) {
      graph.addEdge(vertices[idx], vertices[idx-1]);
    }
  }
}

// Update the graph and remove the visited vertices
function updateGraph() {
  for (var key of graph.AdjList.keys()){
    for (var value of graph.AdjList.get(key)){
      console.log(value);
      
      if(value.wasVisited()) {
        graph.removeEdge(key, value);
      }
    }
  }
}

// Check if the index is inside the grid
function index(i, j) {
  if (i < 0 || j < 0 || i > cols - 1 || j > rows - 1) {
    return -1;
  }
  return i + j * cols;
}

// On mouse pressed event
// Creates the initial point, the final point and the obstacles in the maze.
function mousePressed() {
  clicks = clicks + 1;

  for (let c = 0; c < grid.length; c++) {
    grid[c].click(mouseX, mouseY);
  }
}

// Remove the walls on a specific cell
function removeWalls(a, b) {
  let x = a.getI() - b.getI();
  if (x === 1) {
    a.walls[3] = false;
    b.walls[1] = false;
  } else if (x === -1) {
    a.walls[1] = false;
    b.walls[3] = false;
  }
  let y = a.getJ() - b.getJ();
  if (y === 1) {
    a.walls[0] = false;
    b.walls[2] = false;
  } else if (y === -1) {
    a.walls[2] = false;
    b.walls[0] = false;
  }
}

// Change the color of the final path
function changePathColor() {
  for (var cell of stack){
    if (cell.getType() != "i" && cell.getType() != "f") {
      setCellColor(cell, currentCellColor());
    }
  }
}

// Create a test environment
function createTestEnvironment() {

  // Star point:
  clicks = 1;
  grid[11].clicked();

  // End point:
  clicks = 2;
  grid[60].clicked();

  // Obstacles:
  clicks = 3;
  grid[3].clicked();
  grid[4].clicked();
  grid[20].clicked();
  grid[21].clicked();
  grid[22].clicked();
  grid[23].clicked();
  grid[24].clicked();
  grid[40].clicked();
  grid[28].clicked();
  grid[39].clicked();
  grid[41].clicked();
  grid[42].clicked();
  grid[44].clicked();
  grid[48].clicked();
  grid[52].clicked();
  grid[53].clicked();
  grid[55].clicked();
  grid[58].clicked();
  grid[70].clicked();
  grid[71].clicked();
  grid[65].clicked();
  grid[80].clicked();
  grid[85].clicked();
  grid[86].clicked();
  grid[87].clicked();
  grid[99].clicked();
}

// Get a random cell without considering the start point and the end point
function generateRandomCell(startPoint, endPoint) {
    var cell = Math.floor(Math.random() * (scale*scale));
    return (cell === startPoint || cell === endPoint) ? generateRandomCell(startPoint, endPoint) : cell;
}

// Create a random environment
function createRandomEnvironment() {

  // Star point:
  var startPoint = Math.floor(Math.random() * (scale*scale-1));
    
  clicks = 1;
  grid[startPoint].clicked();
  
  // End point:
  var endPoint = Math.floor(Math.random() * (scale*scale-1));
    
  clicks = 2;
  grid[endPoint].clicked();
  
  var randomPoints = Math.floor((generateRandomCell(startPoint, endPoint) + 2)/1.5);
  
  // Obstacles:
  clicks = 3;
  
  for(var cell = randomPoints; cell > 0; cell--){
    var obstacleCell = generateRandomCell(startPoint, endPoint);
    grid[obstacleCell].clicked();
  }
}